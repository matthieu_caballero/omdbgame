import { Component, OnInit, ViewChild } from '@angular/core';
import { RandomMovieComponent } from './random-movie/random-movie.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  @ViewChild('randomMovieComponent') randomMovieComponent!: RandomMovieComponent;
  hiddenAddToHomeScreen = true;
  deferredPrompt: any;
  title = "omdb2";

  randomId: number = 0;
  searchedId: number = 0;


  ngOnInit() {
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault();
      this.deferredPrompt = e;
      this.hiddenAddToHomeScreen = false;
    });
  }

  addToHomeScreen() {
    this.hiddenAddToHomeScreen = true;
    this.deferredPrompt.prompt();
    this.deferredPrompt.userChoice.then((choiceResult: { outcome: string; }) => {
      if (choiceResult.outcome === 'accepted') {
        console.log('User accepted the A2HS prompt');
      } else {
        console.log('User dismissed the A2HS prompt');
      }
      this.deferredPrompt = null;
    });
  }



  addRandomId($event: any) {
    console.log('addRandomId', $event)
    this.randomId = $event;

  }

  addSearchedId($event: any) {
    console.log('addSearchedId', $event)
    this.searchedId = $event;
    this.checkId();
  }

  checkId() {
    console.log('checkId')
    console.log(this.randomId)
    console.log(this.searchedId)
    if (this.randomId === this.searchedId) {
      console.log('ok michel!');
    }
  }
}

//Bubblewrap pour appli android ?
//Déployer sur Store
