import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize, distinctUntilChanged, filter, mergeAll } from 'rxjs/operators';
import Dexie from "dexie";
import { Observable } from "rxjs";

const API_KEY = "a7cbd9de"

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Input() minLengthTerm!: any;
  @Output() movieId = new EventEmitter<string>();

  searchMoviesCtrl = new FormControl();
  filteredMovies: any;
  isLoading = false;
  errorMsg!: string;
  selectedMovie: any = "";
  private db: Dexie;

  constructor(
    private http: HttpClient
  ) {
    this.db = new Dexie("OMDB2");
    this.db.version(1).stores({
      movie: "Title,Poster,Year"
    });
  }

  onSelected() {
    this.selectedMovie = this.selectedMovie;
    console.log(this.selectedMovie)
    this.movieId.emit(this.selectedMovie.imdbID);
  }

  displayWith(value: any) {
    return value?.Title;
  }

  clearSelection() {
    this.selectedMovie = "";
    this.filteredMovies = [];
  }

  ngOnInit() {
    this.searchMoviesCtrl.valueChanges
      .pipe(
        filter(res => {
          return res !== null && res.length >= this.minLengthTerm
        }),
        distinctUntilChanged(),
        debounceTime(1000),
        tap(() => {
          this.errorMsg = "";
          this.filteredMovies = [];
          this.isLoading = true;
        }),
        switchMap((value: string) => {
          return new Observable((observer) => {
            const cache = this.db.table('movie').orderBy('Title').filter((movie) => {
              return movie.Title.toLowerCase().search(value.toLowerCase()) >= 0;
            }).toArray();

            cache.then((data) => {
              if (data.length > 0) {
                observer.next({ Search: data });
              } else {
                this.http.get('http://www.omdbapi.com/?apikey=' + API_KEY + '&s=' + value).subscribe((data: any) => {
                  if (data['Search']) {
                    this.db.table('movie').bulkAdd(data['Search']);


                  }
                  observer.next(data);
                })
              }
            })
          });
        })
      )
      .subscribe((data: any) => {
        this.isLoading = false;
        if (data['Search'] == undefined) {
          this.errorMsg = data['Error'];
          this.filteredMovies = [];
        } else {
          this.errorMsg = "";
          this.filteredMovies = data['Search'];
        }
      });
  }
}
