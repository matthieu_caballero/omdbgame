import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

const API_KEY = "a7cbd9de"

@Component({
  selector: 'app-random-movie',
  templateUrl: './random-movie.component.html',
  styleUrls: ['./random-movie.component.scss']
})
export class RandomMovieComponent implements OnInit {

  @Output() movieId = new EventEmitter<string>();

  listActor: string[] = [];
  plot: string = '';
  isLoading = true;


  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getRandomMovie();
  }

  getRandomMovie() {
    this.isLoading=true;
    // const randomId = 'tt' + Math.floor(Math.random() * 10000000).toString();
    const randomId = 'tt1592873'
    this.http.get('http://www.omdbapi.com/?apikey=' + API_KEY + '&i=' + randomId + '&plot=full').subscribe(
      (result: any) => {
        console.log(result);
        if (!!result && result.Response === 'True' && result.Plot !== 'N/A' && result.Actors !== 'N/A' && result.Type === "movie") {
          this.plot = result.Plot;
          this.listActor = result.Actors.split(', ');
          console.log(this.plot, this.listActor);
          this.movieId.emit(result.imdbID);
          this.isLoading=false;
        } else {
          this.getRandomMovie();
        }

      }
    );
  }

}
